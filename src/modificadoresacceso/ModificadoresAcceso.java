
package modificadoresacceso;

import paquete1.Clase2;
import paquete2.Clase3;
import paquete2.Clase4;

/**
 *
 * @author tinix
 */
public class ModificadoresAcceso {

    public static void main(String[] args) {
        System.out.println("**** Acceso desde Clase 2 a clase 1 (mismo paquete)****");
        new Clase2().pruebaDesdeClase2();
        
        System.out.println("\n*** Acceso desde Clase 3 a Clase 1(diff package but is not subclass)****");
        new Clase3().pruebaDesdeClase3();
        
        System.out.println("\n*** Acceso desde Clase 4 a Clase 1 (diff package No es subclase)***");
        new Clase4().pruevaDesdeClase4();
    }
    
}
