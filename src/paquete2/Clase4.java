
package paquete2;
import paquete1.Clase1;

public class Clase4 {
    public Clase4(){
    
    }
    
    public void pruevaDesdeClase4(){
    
        Clase1 c1 = new Clase1();
        System.out.println("");
        System.out.println("Atributo Publico" + c1.atrPublico);
        System.out.println("Atributo Protegido : No se pued acceder desde un paquete externo al No ser un subclase");
        System.out.println("Atributo default : No se puede acceder desde un paquete externo");
        System.out.println("Atributo private: Acceso denegado");
                
        new Clase1();
        
        System.out.println("");
        System.out.println("Metodo publico: " + c1.metodoPublico());
        System.out.println("Metodo protegido: No se puede acceder desde un paquete externo al no ser una sub clase");
        System.out.println("Metodo default: No se puede acceder desde un paquete externo");
        System.out.println("Metodo privado acceso denegado");
      
    }
}
