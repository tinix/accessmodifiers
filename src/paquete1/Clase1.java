
package paquete1;

/**
 *
 * @author tinix
 */
public class Clase1 {
    
    public int atrPublico = 5;
    protected int atrProtegido = 6;
    int atrPaquete = 7;
    private int atrPrivado = 8;
    
    public Clase1(){
    }
   
    public Clase1(int i){
        System.out.println("Constructor publico 1");
    }
    
    protected Clase1(int i , int j){
        System.out.println("Contructor protected 2");
    }
    
    Clase1(int i , int j, int k){
        System.out.println("Contructor 3");
    }
    
    
    private Clase1(int i , int j, int k, int l){
        System.out.println("Contructor privado 4");
    } 
    
    public int metodoPublico(){
        return 9;
    }
    
    protected int metodoProtegido(){
        return 10;
    }
    
    int metodoPaquete(){
        return 11;
    }

    private int metodoPrivado(){
        return 12;
    }

}




