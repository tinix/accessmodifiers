
package paquete1;

public class Clase2 {
    
    public Clase2(){
        System.out.println("");
        
       new Clase1(1);
       
       new Clase1(1,2);
       
       new Clase1(1,2,3);
       
        System.out.println("Constructor private: Acceso Negado");
    
    }
    
    public void pruebaDesdeClase2(){
    Clase1 c1 = new Clase1();
        System.out.println("");
        System.out.println("Atributo public :" + c1.atrProtegido);
        System.out.println("Atributo protegido" + c1.atrProtegido);
        System.out.println("Atributo default"+ c1.atrPaquete);
        System.out.println("Atributo private acceso denegado");
        
        System.out.println("");
        System.out.println("Metodo publico" + c1.metodoPublico());
        System.out.println("Metodo protegido"+ c1.metodoProtegido());
        System.out.println("Metodo default paquete"+ c1.atrPaquete);
        System.out.println("Metodo privado acceso denegado");
    }
}
